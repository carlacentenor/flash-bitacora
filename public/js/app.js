$(document).ready(function(){
  $('.modal').modal();
	var $uploadMessage = $('#messageBtn');
	var $newsFeed = $('#newsFeed');
	var $messageOne = $('#messageOne');
	var $messageTitleModal = $('#messageTitleModal');
	var $sendImage = $('#sendImage');
	var $messageTwo = $('#messageTwo');
	var $messageThree = $('#messageThree');
	var $image = $('#image');
	var $imageTitle = $('#imageTitle');
	var $sendVideo = $('#sendVideo');
	var $video = $('#video');
	var $videoTitle = $('#videoTitle');
	var $sendLocation = $('#sendLocation');
	var $locationMessage = $('#locationMessage');
	var $date = $('#date');
	
	function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
   }

   function hour() {
		var currentHour = document.getElementsByClassName('currentHour')[0];
    var d = new Date();
    var h = addZero(d.getHours());
    var m = addZero(d.getMinutes());
    var s = addZero(d.getSeconds());
    currentHour.innerHTML = h + ":" + m + ":" + s;
   }
	
	function renderImage(file) {
		var reader = new FileReader();
		
		reader.onload = function(event) {
			the_url = event.target.result
		}
		reader.readAsDataURL(file);
	}
	
	$image.change(function() {
		console.log(this.files);
		renderImage(this.files[0]);
	})
	
	$video.change(function(){
		console.log(this.files);
		renderImage(this.files[0]);
	})
	
	function geoFindMe() {
		var $locat = document.getElementsByClassName('map')[0];
		var $loadMap = document.getElementsByClassName('loadMap')[0];
		if(!navigator.geolocation) {
			$loadMap.innerHTML = "Geolocation is mot supported by your browser";
			return;
		}
		function success(position) {
			var latitude = position.coords.latitude;
			var longitude = position.coords.longitude;
			$loadMap.innerHTML = "Latitude is " + latitude + "° and Longitude is " + longitude + "°";
			$locat.src = "http://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=300x300&sensor=false";
			console.log(position);
		};
		function error() {
			$loadMap.innerHTML = "Unable to retrive your location";
		};
		$loadMap.innerHTML = "Locating...";
		navigator.geolocation.getCurrentPosition(success, error);
	}
	
	function addLocationPost() {
		$newsFeed.prepend('<div class="row">' +
    '<div class="col s12 m6">' +
      '<div class="card">' +
				 '<div id="ubication" class="card-image">' +
					'<p class="loadMap"></p>' +
          '<a class="btn-floating halfway-fab waves-effect waves-light blue"><i class="material-icons">add</i></a>' +
        '</div>' +
        '<div class="card-content overflow-auto">' +
				'<h3>' + $date.val() + '</h3>' +
         '<p>'+ $locationMessage.val() +'</p>' +
					'<div class="chip">' +
          '<img src="assets/img/clock.jpg" alt="clock"><span class="currentHour"></span></div>' +
        '</div>' +
      '</div>' +
    '</div>' +
  '</div>');
		hour();
		initMap();
	}
	
	function addVideoPost() {
		$newsFeed.prepend('<div class="row">' +
    '<div class="col s12 m6">' +
      '<div class="card">' +
				 '<div class="card-image video-height-container overflow-hidden">' +
          '<video class="video-width" src="' + the_url + '" controls loop>' +
          '</video>' +
        '</div>' +
				'<div class="card-content overflow-auto">' +
				'<span class="card-title">' + $videoTitle.val() + '</span>' +
         '<p>'+ $messageThree.val() +'</p>' +
        '</div>' +
				 '<div class="chip">' +
          '<img src="assets/img/clock.jpg" alt="clock"><span class="currentHour"></span></div>' +
        '</div>' +
      '</div>' +
    '</div>' +
  '</div>');
		hour();
	}
	
	function addImagePost() {
		$newsFeed.prepend('<div class="row">' +
    '<div class="col s12 m6">' +
      '<div class="card">' +
				 '<div class="card-image">' +
          '<img src="'+ the_url+'">' +
          '<span class="card-title">' + $imageTitle.val() + '</span>' +
          '<a class="btn-floating halfway-fab waves-effect waves-light yellow"><i class="material-icons">add</i></a>' +
        '</div>' +
        '<div class="card-content overflow-auto">' +
         '<p>'+ $messageTwo.val() +'</p>' +
					'<div class="chip">' +
          '<img src="assets/img/clock.jpg" alt="clock"><span class="currentHour"></span></div>' +
        '</div>' +
        '</div>' +
      '</div>' +
    '</div>' +
  '</div>');
		hour();
	}
	
	function addPost() {
		$newsFeed.prepend('<div class="row">' +
    '<div class="col s12 m6">' +
      '<div class="card">' +
        '<div class="card-content overflow-auto">' +
         '<h4 id="messageTitle">'+ $messageTitleModal.val() +'</h4>' 
          + '<p>'+ $messageOne.val() +'</p>' +
					'<div class="chip">' +
          '<img src="assets/img/clock.jpg" alt="clock"><span class="currentHour"></span></div>' +
        '</div>' +
					'<a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>' +
        '</div>' +
      '</div>' +
    '</div>' +
  '</div>');
		hour();
	}
	
	$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
	
	$uploadMessage.click(addPost);
	$sendImage.click(addImagePost);
	$sendVideo.click(addVideoPost);
	$sendLocation.click(addLocationPost);
});
         